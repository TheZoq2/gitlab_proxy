use color_eyre::eyre::Context;
use lazy_static::lazy_static;
use log::{error, info, warn, LevelFilter, debug};
use regex::Regex;
use serde::Deserialize;
use tide::{Request, Response, StatusCode};
use tide_acme::{rustls_acme::caches::DirCache, AcmeConfig, TideRustlsExt};

#[derive(Deserialize)]
struct Config {
    pub domain: String,
    pub listen_ip: String,
    pub port: u16,
    pub dev_mode: bool,
    pub contact: String,
}

pub fn translate_to_tar_download(url: &str, gitref: &str) -> tide::Result<String> {
    lazy_static! {
        static ref GITLAB_RE: Regex =
            Regex::new(r"https?://(?<base_url>gitlab.com/(.*/))(?<repo_name>.*)").unwrap();
    }

    if let Some(captures) = GITLAB_RE.captures(url) {
        let base_url = captures.name("base_url").unwrap().as_str();
        let repo_name = captures
            .name("repo_name")
            .unwrap()
            .as_str()
            .replace(".git", "");
        debug!("Looking for {repo_name}");
        let gitref = gitref;
        let result = format!("https://{base_url}/{repo_name}/-/archive/{gitref}/{repo_name}-{gitref}.tar");
        info!("Forwarding request to result");
        Ok(result)
    } else {
        Err(tide::Error::from_str(
            StatusCode::NotFound,
            "Failed to convert to a zip download url. It must be a gitlab repo URL",
        ))
    }
}

async fn forward_request(req: Request<()>) -> tide::Result {
    info!("Received request for {}", req.url());
    let pairs = req
        .url()
        .query_pairs();

    let url = pairs.clone()
        .find(|(param, _value)| param == "url");
    let gitref = pairs.clone()
        .find(|(param, _value)| param == "gitref");

    let (Some((_, url)), Some((_, gitref))) = (url, gitref)
    else {
        return Ok(Response::builder(404).body("Missing url param").build());
    };

    let url = translate_to_tar_download(&url, &gitref)?;

    let response = reqwest::get(&url)
        .await
        .map_err(|e| {
            error!("Failed to get({url})");
            e
        })?
        .bytes()
        .await
        .map_err(|e| {
            error!("Failed to body of {url} as bytes");
            e
        })?;

    Ok(Response::builder(200)
        .header("Access-Control-Allow-Origin", "*")
        .body(response.to_vec())
        .build())
}

#[tokio::main]
async fn main() -> color_eyre::Result<()> {
    env_logger::builder()
        .filter_level(LevelFilter::Debug)
        .init();

    let config_file = "gitlab_proxy.toml";
    let config = tokio::fs::read_to_string(config_file)
        .await
        .with_context(|| format!("Failed to read {config_file}"))
        .and_then(|content| {
            toml::from_str::<Config>(&content)
                .with_context(|| format!("Failed to deserialize {config_file}"))
        })?;

    if !config.dev_mode {
        warn!("Running in dev-mode.")
    }

    let acme_config = AcmeConfig::new(vec![config.domain])
        .contact_push(config.contact)
        .cache(DirCache::new("tide-acme-cache-dir"))
        .directory_lets_encrypt(!config.dev_mode);

    info!("Starting server");
    let mut app = tide::new();
    app.at("/").get(forward_request);
    app.listen(
        tide_rustls::TlsListener::build()
            .addrs(format!(
                "{ip}:{port}",
                ip = config.listen_ip,
                port = config.port
            ))
            .acme(acme_config),
    )
    .await?;

    Ok(())
}
